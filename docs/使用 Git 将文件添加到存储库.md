# 使用 Git 将文件添加到存储库

要从命令行添加新文件，请执行以下操作：

1. 打开终端。

2. 更改目录，直到您进入项目的文件夹。

   ```shell
   cd my-project
   ```

3. 选择要使用的 Git 分支。

   - 要创建分支：`git checkout -b <branchname>`
   - 要切换到现有分支，请执行以下操作：`git checkout <branchname>`

4. 将要添加的文件复制到要添加的目录中。

5. 确认您的文件位于以下目录中：

   - 窗户：`dir`
   - 所有其他操作系统：`ls`

   应显示文件名。

6. 检查文件的状态：

   ```shell
   git status
   ```

   文件名应为红色。该文件位于您的文件系统中，但 Git 尚未跟踪它。

7. 告诉 Git 跟踪文件：

   ```shell
   git add <filename>
   ```

8. 再次检查文件的状态：

   ```shell
   git status
   ```

   文件名应为绿色。该文件由 Git 在本地跟踪，但 没有被承诺和推动。

9. 将文件提交到项目的 Git 存储库的本地副本：

   ```shell
   git commit -m "本次提交的描述信息"
   ```

## 将文件添加到上次提交

```shell
git add <filename>
git commit --amend
```

如果不想编辑提交，请追加到命令中 消息。`--no-edit``commit`
