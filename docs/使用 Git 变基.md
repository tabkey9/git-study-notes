## 使用 Git 变基

1. 如果存在合并冲突：

   1. 修复编辑器中的冲突。

   2. 添加文件：

      ```shell
      git add .
      ```

   3. 继续变基：

      ```shell
      git rebase --continue
      ```

2. 强制将更改推送到目标分支，同时保护其他人的提交：

   ```shell
   git push origin my-branch --force-with-lease
   ```
