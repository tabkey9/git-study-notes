## 配置 Git

若要从计算机开始使用 Git，必须输入凭据 表明自己是作品的作者。全名和电子邮件地址 应该与您在 GitLab 中使用的匹配。

1. 在您的终端中，添加您的全名。例如：

   ```shell
   git config --global user.name "Sidney Jones"
   ```

2. 添加您的电子邮件地址。例如：

   ```shell
   git config --global user.email "your_email_address@example.com"
   ```

3. 若要检查配置，请运行：

   ```shell
   git config --global --list
   ```

   该选项告诉 Git 始终将此信息用于您在系统上执行的任何操作。 如果省略或使用 ，则配置仅适用于当前 存储 库。`--global``--global``--local`

   
