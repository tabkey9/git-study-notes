## 命令行 Git

### 创建分支

要创建功能分支，请执行以下操作：

```shell
git checkout -b <name-of-branch>
```

GitLab 强制执行[分支命名规则](https://docs.gitlab.com/ee/user/project/repository/branches/index.html#name-your-branch)以防止出现问题，并提供[分支命名模式](https://docs.gitlab.com/ee/user/project/repository/branches/index.html#prefix-branch-names-with-issue-numbers)以简化合并请求的创建。

### 切换到分支

Git 中的所有工作都在分支中完成。 您可以在分支之间切换以查看文件的状态并在该分支中工作。

要切换到现有分支，请执行以下操作：

```shell
git checkout <name-of-branch>
```

例如，要更改为分支：`main`

```shell
git checkout main
```

### 添加和提交本地更改

当您准备好将更改写入分支时，您可以提交 他们。提交包括一个注释，该注释记录有关 变化，并且通常成为分支的新尖端。

Git 不会自动包含您移动、更改或 在提交中删除。这样可以防止您意外地包含 更改或文件，例如临时目录。要将更改包含在 提交，用 .`git add`

要暂存和提交更改，请执行以下操作：

1. 在存储库中，对于要添加的每个文件或目录，运行 。`git add <file name or path>`

   若要暂存当前工作目录中的所有文件，请运行 。`git add .`

2. 确认文件已添加到暂存：

   ```shell
   git status
   ```

   文件以绿色显示。

3. 要提交暂存文件，请执行以下操作：

   ```shell
   git commit -m "本次提交的描述信息"
   ```

这些更改将提交到分支。

### 提交所有更改

您可以暂存所有更改，并使用一个命令提交它们：

```shell
git commit -a -m "本次提交的描述信息"
```

请注意，您的提交不包括您不想记录的文件 到远程存储库。通常，请始终检查您的状态 本地存储库，然后再提交更改。

### 将更改发送到 GitLab

要将所有本地更改推送到远程存储库，请执行以下操作：

```shell
git push <remote> <name-of-branch>
```

例如，要将本地提交推送到远程的分支，请执行以下操作：`main``origin`

```shell
git push origin main
```

有时 Git 不允许你推送到存储库。相反 您必须[强制更新](https://docs.gitlab.com/ee/topics/git/git_rebase.html#force-pushing)。

## 删除更改

如果要撤消更改，可以使用 Git 命令返回到存储库的早期版本。

删除更改通常是一种不可逆的破坏性操作。如果 可能，您应该添加其他提交，而不是恢复旧的 的。

### 覆盖未提交的更改

您可以用作丢弃跟踪的快捷方式， 未提交的更改。`git checkout`

要放弃对跟踪文件的所有更改：

```shell
git checkout .
```

您的更改将被分支中的最新提交覆盖。 未跟踪的文件不受影响。

### 重置更改和提交



如果已将提交推送到远程，请不要重置提交 存储 库。

如果您暂存更改，然后决定不提交， 您可能希望取消暂存更改。要取消暂存更改，请执行以下操作：`git add`

- 从存储库中，运行 。`git reset`

如果更改已提交（但未推送到远程 repository），您可以重置您的提交：

```shell
git reset HEAD~<number>
```

这里是要撤消的提交数。 例如，如果只想撤消最新的提交：`<number>`

```shell
git rest HEAD~1
```

提交将重置，任何更改都将保留在本地存储库中。

若要详细了解撤消更改的不同方法，请参阅 [Git 撤消 Things 文档](https://git-scm.com/book/en/v2/Git-Basics-Undoing-Things)。

## 将分支与默认分支合并

当您准备好将更改添加到 默认分支，则将功能分支合并到其中：

```shell
git checkout <default-branch>
git merge <feature-branch>
```

在 GitLab 中，您通常使用[合并请求](https://docs.gitlab.com/ee/user/project/merge_requests/index.html)来合并更改，而不是使用命令行。
