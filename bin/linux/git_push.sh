#!/bin/bash  
  
# 输出当前工作目录  
echo $(pwd)  
echo "拉取最新代码"  
git pull origin main  
  
# 检查文件状态  
echo "检查文件状态"  
git status  
  
# 询问用户是否要提交代码，并等待用户按回车键继续  
read -p "是否要提交代码？（如需退出请按 Ctrl+C）" -n 1 -s  
echo  
  
# 如果用户没有按 Ctrl+C，则继续  
if [ $? -ne 130 ]; then  
    echo "告知 Git 追踪文件"  
    git add .  
      
    # 再次检查文件状态  
    echo "检查文件状态"  
    git status  
      
    # 等待用户输入提交描述，直到输入不为空  
    while true; do  
        read -p "在此处描述您提交的原因：" inputDescribe  
          
        # 检查是否输入了提交描述  
        if [ -n "$inputDescribe" ]; then  
            break  # 如果输入不为空，则跳出循环  
        else  
            echo "没有输入有效的提交描述，请重新输入。"  
        fi  
    done  
      
    echo "将文件变更提交到 Git 存储库的本地副本"  
    git commit -m "$inputDescribe"  
      
    echo "将变更从存储库副本推送到远程存储库"  
    git push origin main  
fi  
  
echo "脚本执行结束。"