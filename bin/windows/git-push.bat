@echo off
echo %cd%
echo 拉取最新代码
git pull origin main
echo 检查文件状态
git status
echo 是否要提交代码？（如需退出请关闭本窗口） & pause 
echo 告知 Git追踪文件
git add .
echo 检查文件状态
git status
echo 将文件变更提交到 Git存储库的本地副本
echo 等待输入...
:input_describe
set /p inputDescribe="在此处描述您提交的原因："
echo 描述: %inputDescribe%  
if not "%inputDescribe%"=="" (  
	git commit -m %inputDescribe%
	echo 将变更从存储库副本推送到远程存储库
	git push origin main
) else (  
	echo 输入有误，请重新输入。  
	goto :input_describe
)
echo 脚本执行结束。（按任意键退出） & pause 
